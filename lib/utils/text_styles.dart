import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tomisha_test/utils/colors.dart';

mixin TextStyles {
  static TextStyle get textStyle => GoogleFonts.lato(
        fontSize: 17,
        fontWeight: FontWeight.normal,
        color: AppColors.textColor,
        decoration: TextDecoration.none,
      );

  static TextStyle get secondaryTextStyle => GoogleFonts.epilogue(
        fontSize: 14,
        fontWeight: FontWeight.normal,
        decoration: TextDecoration.none,
      );
}
