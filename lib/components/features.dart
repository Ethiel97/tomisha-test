import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:tomisha_test/utils/text_styles.dart';

class Features extends StatelessWidget {
  const Features({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 40,
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(36.0),
            child: Text(
              "Drei einfache Schritte\nzu deinem neuen Job",
              textAlign: TextAlign.start,
              style: TextStyles.textStyle.apply(fontSizeDelta: 10),
            ),
          ),
          const SizedBox(
            height: 120,
          ),
          const Feature1(),
          SizedBox(
            height: context.isPhone ? 120 : 200,
          ),
          const Feature2(),
          const SizedBox(
            height: 100,
          ),
          const Feature3(),
          const SizedBox(
            height: 300,
          )
        ],
      ),
    );
  }
}

class Feature1 extends StatelessWidget {
  const Feature1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Column(
        children: [
          if (context.isSmallTablet)
            Padding(
              padding:
                  EdgeInsets.only(left: Get.width / 6, right: Get.width / 4),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  const FeatureText(
                    number: '1.',
                    text: 'Erstellen dein Lebenslauf',
                  ),
                  const SizedBox(
                    width: 100,
                  ),
                  Flexible(
                    child: SvgPicture.asset(
                      'assets/images/undraw_Profile_data_re_v81r.svg',
                      fit: BoxFit.contain,
                      height: 200,
                      width: 200,
                    ),
                  ),
                ],
              ),
            )
          else if (context.isPhone)
            Column(
              children: [
                SvgPicture.asset(
                  'assets/images/undraw_Profile_data_re_v81r.svg',
                  fit: BoxFit.contain,
                  height: 200,
                  width: 200,
                ),
                const SizedBox(
                  height: 20,
                ),
                const FeatureText(
                  number: '1.',
                  text: 'Erstellen dein Lebenslauf',
                ),
              ],
            )
        ],
      ),
    );
  }
}

class Feature2 extends StatelessWidget {
  const Feature2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => ClipPath(
        clipper: CustomShape(),
        child: Container(
          padding: const EdgeInsets.all(50),
          decoration: const BoxDecoration(
            gradient: LinearGradient(colors: [
              Color(0xffEBF4FF),
              Color(0xffE6FFFA),
            ]),
          ),
          child: Column(
            children: [
              if (context.isSmallTablet)
                Padding(
                  padding: const EdgeInsets.all(12),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      SvgPicture.asset(
                        'assets/images/undraw_task_31wc.svg',
                        fit: BoxFit.contain,
                        height: 200,
                        width: 200,
                      ),
                      const SizedBox(
                        width: 100,
                      ),
                      const Flexible(
                        child: FeatureText(
                          number: '2.',
                          text: 'Erstellen dein Lebenslauf',
                        ),
                      ),
                    ],
                  ),
                )
              else if (context.isPhone)
                Column(
                  children: [
                    const FeatureText(
                        number: '2.', text: 'Erstellen dein Lebenslauf'),
                    const SizedBox(
                      height: 20,
                    ),
                    SvgPicture.asset(
                      'assets/images/undraw_task_31wc.svg',
                      fit: BoxFit.contain,
                      height: 200,
                      width: 200,
                    ),
                  ],
                )
            ],
          ),
        ),
      );
}

class Feature3 extends StatelessWidget {
  const Feature3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Column(
        children: [
          if (context.isSmallTablet)
            Padding(
              padding: const EdgeInsets.all(12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  const FeatureText(
                      number: '3.', text: 'Mit nur einem Klick bewerben'),
                  const SizedBox(
                    width: 100,
                  ),
                  SvgPicture.asset(
                    'assets/images/undraw_personal_file_222m.svg',
                    fit: BoxFit.contain,
                    height: 200,
                    width: 200,
                  )
                ],
              ),
            )
          else if (context.isPhone)
            Column(
              children: [
                const FeatureText(
                    number: '3.', text: 'Erstellen dein Lebenslauf'),
                const SizedBox(
                  height: 20,
                ),
                SvgPicture.asset(
                  'assets/images/undraw_personal_file_222m.svg',
                  fit: BoxFit.contain,
                  height: 200,
                  width: 200,
                ),
              ],
            )
        ],
      ),
    );
  }
}

class FeatureText extends StatelessWidget {
  final String number;
  final String text;

  const FeatureText({
    Key? key,
    required this.number,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
          text: number,
          style: TextStyles.textStyle.apply(
            fontSizeDelta: 96,
            fontWeightDelta: 2,
          ),
          children: [
            TextSpan(
              text: text,
              style: TextStyles.textStyle.apply(
                fontSizeDelta: 1,
              ),
            )
          ]),
    );
  }
}

//Copy this CustomPainter code to the Bottom of the File
class CustomShape extends CustomClipper<Path> {
  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width, size.height * 0.03178636);
    path_0.cubicTo(
        size.width * 0.6194444,
        size.height * -0.1058393,
        size.width * 0.6083333,
        size.height * 0.2621935,
        0,
        size.height * 0.03178636);
    path_0.lineTo(0, size.height * 0.8915630);
    path_0.cubicTo(
        size.width * 0.3500000,
        size.height * 0.8281613,
        size.width * 0.7746181,
        size.height * 1.199205,
        size.width,
        size.height * 0.8436261);
    path_0.lineTo(size.width, size.height * 0.03178636);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.shader = ui.Gradient.linear(const Offset(0, 0),
        Offset(size.width * 0.3664236, size.height * 1.633578), [
      const Color(0xffEBF4FF).withOpacity(1),
      const Color(0xffE6FFFA).withOpacity(1)
    ], [
      0,
      1
    ]);
    canvas.drawPath(path_0, paint_0_fill);
  }

  @override
  Path getClip(Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width, size.height * 0.03178636);
    path_0.cubicTo(
        size.width * 0.6194444,
        size.height * -0.1058393,
        size.width * 0.6083333,
        size.height * 0.2621935,
        0,
        size.height * 0.03178636);
    path_0.lineTo(0, size.height * 0.8915630);
    path_0.cubicTo(
        size.width * 0.3500000,
        size.height * 0.8281613,
        size.width * 0.7746181,
        size.height * 1.199205,
        size.width,
        size.height * 0.8436261);
    path_0.lineTo(size.width, size.height * 0.03178636);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.shader = ui.Gradient.linear(const Offset(0, 0),
        Offset(size.width * 0.3664236, size.height * 1.633578), [
      const Color(0xffEBF4FF).withOpacity(1),
      const Color(0xffE6FFFA).withOpacity(1)
    ], [
      0,
      1
    ]);

    return path_0;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}
