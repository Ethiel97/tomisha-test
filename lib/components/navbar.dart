import 'package:flutter/material.dart';
import 'package:tomisha_test/utils/text_styles.dart';

class Navbar extends StatelessWidget {
  const Navbar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Material(
        elevation: 4.0,
        shadowColor: Colors.black87,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(
              12,
            ),
            bottomLeft: Radius.circular(
              12,
            ),
          ),
        ),
        child: Container(
          padding: const EdgeInsets.symmetric(
            vertical: 14,
            horizontal: 20,
          ),
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(
                12,
              ),
              bottomLeft: Radius.circular(
                12,
              ),
            ),
            color: Colors.white,
            /* boxShadow: [
              BoxShadow(
                offset: Offset(0, 6),
                color: Colors.grey.withOpacity(.02),
                blurRadius: 12,
              )
            ],*/
          ),
          child: Row(
            children: [
              const Spacer(),
              Text(
                "Login",
                style: TextStyles.textStyle.apply(
                  color: const Color(
                    0xff319795,
                  ),
                ),
              ),
            ],
          ),
        ),
      );
}
