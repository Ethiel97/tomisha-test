import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:tomisha_test/utils/text_styles.dart';
import 'package:tomisha_test/widgets/gradient_button.dart';

class Header extends StatelessWidget {
  const Header({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => ClipPath(
        clipper: CustomShape(),
        child: Container(
          decoration: const BoxDecoration(
            color: Color(0xffE6FFFA),
          ),
          padding: EdgeInsets.symmetric(
            vertical: context.isPhone ? 30 : 110,
            horizontal: 18,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              if (context.isPhone)
                Column(
                  children: [
                    Text(
                      "Deine Job\n Website",
                      style: TextStyles.textStyle.apply(
                        fontSizeDelta: context.isPhone ? 20 : 65,
                        fontWeightDelta: 10,
                      ),
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: SvgPicture.asset(
                        "assets/images/undraw_agreement_aajr.svg",
                        height: 200,
                        width: 200,
                      ),
                    ),
                  ],
                ),
              if (context.isSmallTablet)
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Deine Job\nWebsite",
                          textAlign: TextAlign.start,
                          style: TextStyles.textStyle.apply(
                            fontSizeDelta: 40,
                            fontWeightDelta: 10,
                          ),
                        ),
                        const SizedBox(
                          height: 60,
                        ),
                        GradientButton(
                          text: "Kostenlos Registrieren",
                          onTap: () {},
                        )
                      ],
                    ),
                    const SizedBox(
                      width: 150,
                    ),
                    Flexible(
                      child: CircleAvatar(
                        radius: 200,
                        backgroundColor: Colors.white,
                        child: SvgPicture.asset(
                          "assets/images/undraw_agreement_aajr.svg",
                          height: 300,
                          width: 300,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ],
                ),
            ],
          ),
        ),
      );
}

//Add this CustomPaint widget to the Widget Tree
/*
CustomPaint(
size: Size(WIDTH, (WIDTH*0.6263888888888889).toDouble()), //You can Replace [WIDTH] with your desired width for Custom Paint and height will be calculated automatically
painter: RPSCustomPainter(),
)
*/

//Copy this CustomPainter code to the Bottom of the File
class CustomShape extends CustomClipper<Path> {
  @override
  bool shouldReclip(covariant CustomClipper oldClipper) {
    return true;
  }

  @override
  ui.Path getClip(ui.Size size) {
    Path path_0 = Path();
    path_0.moveTo(0, size.height);
    path_0.lineTo(0, 0);
    path_0.lineTo(size.width, 0);
    path_0.lineTo(size.width, size.height * 0.8699257);
    path_0.cubicTo(size.width * 0.7232292, size.height * 0.8417561,
        size.width * 0.2086229, size.height, 0, size.height);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.shader = ui.Gradient.linear(
        Offset(0, 0),
        Offset(size.width * 0.5635931, size.height * 1.436408),
        [Color(0xffEBF4FF).withOpacity(1), Color(0xffE6FFFA).withOpacity(1)],
        [0, 1]);
    // canvas.drawPath(path_0, paint_0_fill);

    // path_0.

    return path_0;
  }
}
