import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:tomisha_test/components/features.dart';
import 'package:tomisha_test/components/header.dart';
import 'package:tomisha_test/components/navbar.dart';
import 'package:tomisha_test/widgets/gradient_button.dart';

void main() {
  runApp(const GetMaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'Flutter Demo',
    home: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            ListView(
              controller: scrollController,
              children: [
                const Navbar(),
                const SizedBox(
                  height: 2,
                ),
                const Header(),
                Stack(
                  children: [
                    const Features(),
                    if (context.isLargeTablet) ...[
                      Positioned(
                        top: 500,
                        left: 310,
                        child: SvgPicture.asset(
                          'assets/images/Gruppe_1821.svg',
                          fit: BoxFit.contain,
                        ),
                      ),
                      Positioned(
                        top: 1000,
                        right: 540,
                        child: SvgPicture.asset(
                          'assets/images/Gruppe_1822.svg',
                          fit: BoxFit.contain,
                        ),
                      ),
                    ]
                  ],
                ),
              ],
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Material(
                elevation: 4.0,
                shadowColor: Colors.black87,
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(
                      12,
                    ),
                    topLeft: Radius.circular(
                      12,
                    ),
                  ),
                ),
                child: Container(
                  height: 100,
                  padding: const EdgeInsets.symmetric(
                    horizontal: 24,
                    vertical: 18,
                  ),
                  child: GradientButton(
                    text: 'Kostenlos Registrieren',
                    onTap: () {
                      scrollController.animateTo(
                        0,
                        duration: Duration(milliseconds: 300),
                        curve: Curves.easeInOut,
                      );
                    },
                  ),
                ),
              ),
            )
          ],
        ),
      );
}
