import 'package:flutter/material.dart';
import 'package:tomisha_test/utils/text_styles.dart';

class GradientButton extends StatelessWidget {
  final String text;
  VoidCallback? onTap;

  GradientButton({
    Key? key,
    required this.text,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => GestureDetector(
        onTap: onTap,
        child: Material(
          borderRadius: BorderRadius.circular(12),
          elevation: 12.0,
          child: Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.symmetric(
              vertical: 12,
              horizontal: 48,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              gradient: const LinearGradient(
                colors: [
                  Color(0xff2f9496),
                  Color(0xff267dc2),
                ],
              ),
            ),
            child: Text(
              text,
              style: TextStyles.textStyle.apply(
                color: Colors.white,
              ),
            ),
          ),
        ),
      );
}
